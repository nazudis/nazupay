# NAZUPAY

#### Learn API Payment Gateway.

---

## Introduction

[Nazupay](https://pay-dev.nazuapp.site) is FREE Fake API Payment Gateway with method VA (Virtual Account). For developer who want to learn about payment. I create this by my experience to handle Payment, I just implement as simple as I can. I hope after you use this API, you all have a bit an idea how the Real Payment Works.

## Get Started

To get stared and use this API for you to practice, and if you have a questions. Feel free to chat with me. Link in the [Contact](#contact) section Bellow.

## Features

- Inquiry VA
- Create VA
- Notification Callback
- Payment **[Simulator](https://pay-dev.nazuapp.site/public/simulator)**

## FAQ

- Is this free ?
  Yes! This is a free API. From Developer For Developer :)
- How to use this API ?
  Please refer to the [Documentation](#documentation).
- Is money required for payment ?
  Nope! This is just simulation. You can use Payment Simulation for make payment.
- How if i got an error that im not understand ?
  Feel free to chat me :)

## Documentation

Read the **[Documentation](https://pay-dev.nazuapp.site/public/docs 'Nazupay Documentation')**.

## Contact

**[LinkedIn](https://www.linkedin.com/in/akmal-fauzan-799212180/)**
**[WhatsApp](https://wa.me/+6289625201030)**
