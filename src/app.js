/* eslint-disable node/no-unsupported-features/es-syntax */
const path = require('path')
const AutoLoad = require('fastify-autoload')
const dbconnect = require('./dbconnect')
const AppJson = require('../package.json')

module.exports = async function (fastify, opts) {
  // Place here your custom code!
  global.rootdir = __dirname
  dbconnect(fastify)
  Object.assign(opts, { AppJson })
  // Do not touch the following lines

  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: { ...opts },
  })

  // This loads all plugins defined in routes
  // define your routes in one of these
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'routes'),
    options: { ...opts },
  })
}
