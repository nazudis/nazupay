const mongoose = require('mongoose')

module.exports = async (fastify) => {
  mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })

  const db = mongoose.connection

  db.once('open', () =>
    fastify.log.info('Database connection has been established')
  )

  db.on('error', () => {
    fastify.log.error('Cannot connect to Database')
    process.exit(1)
  })
}
