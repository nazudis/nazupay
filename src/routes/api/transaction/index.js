const Joi = require('joi')
const { default: axios } = require('axios')

module.exports = async (fastify) => {
  const {
    ResponseSuccess,
    ResponseError,
    TrxModel,
    VaModel,
    genTrxId,
    cleansData,
    dayjs,
  } = fastify

  fastify.post('/create', async (req) => {
    const schema = Joi.object({
      amount: Joi.number().required(),
      va_number: Joi.number().required(),
    })
    const { error, value } = schema.validate(req.body)
    if (error) throw ResponseError('022', error.message, value, 422)

    const va = await VaModel.findOne({ va_number: value.va_number })
    if (!va)
      throw ResponseError('004', 'Invalid VA. VA number not found.', value, 404)

    if (va.va_type === 'M' && va.amount > value.amount)
      throw ResponseError('022', `Minimum payment is ${va.amount}`, value, 422)

    const today = dayjs().format()
    const transaction = await TrxModel.create({
      client_name: va.client_name,
      trx_status: 'SUCCESS',
      trx_id: await genTrxId(va.partner_id, 'invoice'),
      trx_amount: value.amount,
      trx_time: today,
      va_number: va.va_number,
      partner_id: va.partner_id,
      partner_trx_id: va.partner_trx_id,
      description: va.description,
    })

    const data = cleansData(transaction.toObject())

    if (va.callback) {
      const resultsCallback = await axios({
        url: va.callback,
        method: 'POST',
        data,
        headers: {
          'Content-Type': 'application/json',
          'Bypass-Tunnel-Reminder': 'OK',
        },
      })
      console.log(resultsCallback.data, 'RESULT CALLBACK')
    }

    return ResponseSuccess('000', 'Payment Success', data)
  })
}
