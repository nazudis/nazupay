module.exports = async (fastify) => {
  const {
    genPassword,
    genApiKey,
    genPartnerId,
    dayjs,
    PartnerModel,
    ResponseError,
    ResponseSuccess,
    cleansData,
  } = fastify

  // Register User
  fastify.post('/', async (req) => {
    const { company, password, email } = req.body

    let partner = await PartnerModel.findOne({
      $or: [{ company }, { email }],
    })
    if (partner) {
      throw ResponseError(
        '022',
        'Failed to create partner. company or email has been taken.',
        { company, email },
        422
      )
    }

    const data = {
      email,
      company,
      password: genPassword(password),
      expired_key: dayjs().add(30, 'day').format(),
      partner_id: await genPartnerId(),
    }

    partner = await PartnerModel.create(data)
    const apikey = genApiKey(partner.id)

    return ResponseSuccess('000', 'Success create partner.', {
      company: partner.company,
      partner_id: partner.partner_id,
      apikey,
    })
  })

  fastify.get('/detail/:id', async (req) => {
    const { id } = req.params

    let partner = await PartnerModel.findById(id)
    if (!partner) throw ResponseError('004', 'Partner not found', { id })

    partner = cleansData(partner.toObject())

    return ResponseSuccess('000', 'Partner found.', partner)
  })

  fastify.get('/', async (req) => {
    const { limit, page, sortBy, sortType, search } = req.query

    const searchQuery = { company: { $regex: `.*${search}.*` } }
    const total = await PartnerModel.countDocuments()
    const filtered = await PartnerModel.countDocuments(searchQuery)
    let partners = await PartnerModel.find(searchQuery)
      .limit(parseInt(limit, 10))
      .skip((page - 1) * limit)
      .sort({ [sortBy]: sortType === 'asc' ? 1 : -1 })

    partners = partners.map((obj) => cleansData(obj.toObject()))

    const msg = filtered ? 'Success get Partners.' : 'No data.'

    return ResponseSuccess('000', msg, { total, filtered, partners })
  })
}
