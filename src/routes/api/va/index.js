const Joi = require('joi')

module.exports = async (fastify) => {
  const {
    VaModel,
    ResponseSuccess,
    ResponseError,
    genVA,
    genTrxId,
    cleansData,
  } = fastify

  fastify.post('/create-static', async (req) => {
    const { partner } = req

    const schema = Joi.object({
      type: Joi.string().allow('O', 'M').required(),
      client_name: Joi.string()
        .allow('')
        .optional()
        .default(`Customer ${partner.company}`),
      amount: Joi.number().min(0).required(),
      partner_trx_id: Joi.string()
        .optional()
        .default(
          await genTrxId(
            partner.partner_id,
            'partner',
            partner.company.substr(0, 3).toUpperCase()
          )
        ),
      description: Joi.string()
        .allow('')
        .optional()
        .default(`Payment For ${partner.company}`),
      exp_time: Joi.date().optional().default(fastify.dayjs().add(1, 'day')),
      callback: Joi.string().uri().optional(),
    })

    const { error, value } = schema.validate(req.body)
    if (error) {
      throw ResponseError('022', error.message, value, 422)
    }

    let va = await VaModel.findOne({ partner_trx_id: value.partner_trx_id })
    if (va)
      throw ResponseError('002', 'Reject. Duplicate Transaction Id', value, 400)

    if (value.type === 'O' && value.amount > 0) {
      throw ResponseError(
        '022',
        'The amount must be set to 0 if the type of VA is open payment.',
        422
      )
    }

    va = await VaModel.create({
      va_type: value.type,
      client_name: value.client_name,
      va_number: await genVA(partner.partner_id),
      amount: value.amount,
      partner_id: partner.partner_id,
      partner_trx_id: value.partner_trx_id,
      description: value.description,
      exp_time: value.exp_time,
      callback: value.callback,
    })

    const data = cleansData(va.toObject())

    return ResponseSuccess('000', 'Success', data)
  })

  fastify.get('/detail/:vanumber', async (req) => {
    const schema = Joi.object({
      vanumber: Joi.number().required(),
    })
    const { error, value } = schema.validate(req.params)
    if (error) throw ResponseError('004', error.message, value, 400)

    const va = await VaModel.findOne({ va_number: value.vanumber })
    if (!va)
      throw ResponseError('004', 'Reject. VA Number not found', value, 400)

    const data = cleansData(va.toObject())

    return ResponseSuccess('000', 'Success', data)
  })
}
