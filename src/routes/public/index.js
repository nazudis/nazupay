module.exports = async function (fastify) {
  fastify.get('/simulator', (req, res) => {
    const cookie = JSON.stringify({
      apikey: process.env.API_KEY,
      partnerId: process.env.PARTNER_ID,
    })

    const keys = Buffer.from(cookie).toString('hex')

    res.setCookie('keys', keys, { secure: true })
    res.view('/simulator')
  })

  fastify.get('/docs', (req, res) => {
    res.redirect(
      'https://documenter.getpostman.com/view/12121707/Tzz5sdUN#acbc87b1-657f-42e2-8190-de02227982e3'
    )
  })
}
