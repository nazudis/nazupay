const fs = require('fs')
const path = require('path')
const MarkdownIt = require('markdown-it')

module.exports = async function (fastify, opts) {
  const { AppJson } = opts
  fastify.get('/', async () => ({
    app: AppJson.name,
    version: AppJson.version,
    description: AppJson.description,
    message: 'Welcome to FAPI! To get start please click Link bellow',
    readme: 'https://pay-dev.nazuapp.site/readme',
  }))

  fastify.get('/readme', (req, res) => {
    const md = new MarkdownIt()
    let html = fs.readFileSync(
      path.join(__dirname, '../public/views/header.html'),
      'utf-8'
    )
    const file = fs.readFileSync(
      path.join(__dirname, '../../README.md'),
      'utf-8'
    )
    const content = md.render(file)
    html = html
      .replace('CONTENT', content)
      .replace('<h2>Contact</h2>', '<h2 id="contact">Contact</h2>')
      .replace(
        '<h2>Documentation</h2>',
        '<h2 id="documentation">Documentation</h2>'
      )

    res.header('Content-Type', 'text/html')
    res.send(html)
  })
}
