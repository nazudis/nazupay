const fp = require('fastify-plugin')

module.exports = fp(async (fastify) => {
  const { ResponseError } = fastify

  fastify.addHook('preHandler', async (req) => {
    const { url, method } = req

    if (url === '/api/partner' && method.toLowerCase() === 'post') {
      const authkey = req.headers['x-auth-key']
      if (authkey !== process.env.ENC_KEY) {
        throw ResponseError('003', 'FORBIDEN!', {}, 403)
      }
    } else if (url.indexOf('api') === 1) {
      const apikey = req.headers['x-api-key']
      const partnerId = req.headers['x-partner-id']

      if (!apikey)
        throw ResponseError('003', 'Reject. Api Key not defined', {}, 403)

      if (!partnerId)
        throw ResponseError('003', 'Reject. Partner ID not defined', {}, 403)

      const id = fastify.decodeApiKey(apikey)
      const partner = await fastify.PartnerModel.findById(id).lean()
      if (!partner)
        throw ResponseError(
          '001',
          'Reject. Unauthorized! Invalid API KEY.',
          {},
          401
        )

      if (partnerId !== partner.partner_id)
        throw ResponseError(
          '001',
          'Reject. Unauthorized! Invalid Partner ID.',
          {},
          401
        )

      const today = +fastify.dayjs().toDate()
      const expkey = +fastify.dayjs(partner.expired_key).toDate()
      if (today > expkey)
        throw ResponseError(
          '001',
          'Reject. Unauthorized! Expired API KEY.',
          {},
          401
        )

      req.partner = partner
    }
  })

  fastify.addHook('onRequest', (req, _, done) => {
    req.partner = {}
    return done()
  })
})
