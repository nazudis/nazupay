const fp = require('fastify-plugin')
const path = require('path')

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(async (fastify) => {
  fastify.decorate('someSupport', () => 'hugs')

  fastify.setErrorHandler((error, _, rep) => {
    fastify.log.error(error)
    rep.status(error.statusCode || 500).send(error.result || error)
  })

  fastify.register(require('point-of-view'), {
    engine: { pug: require('pug') },
    root: path.join(global.rootdir, 'public/views'),
    includeViewExtension: true,
  })

  fastify.register(require('fastify-static'), {
    root: path.join(global.rootdir, 'public'),
  })

  fastify.register(require('fastify-cookie'))
})
