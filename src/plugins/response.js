/* eslint-disable node/no-unsupported-features/es-syntax */
const fp = require('fastify-plugin')

module.exports = fp(async (fastify) => {
  fastify.decorate('ResponseSuccess', (code = '000', message = '', data) => ({
    status: { code, message },
    ...data,
  }))

  fastify.decorate(
    'ResponseError',
    (code = '999', message = '', data, errorCode, stack) => {
      const error = {
        statusCode: errorCode,
        stack,
        message,
        result: {
          status: { code, message },
          ...data,
        },
      }

      return error
    }
  )
})
