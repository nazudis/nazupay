const fp = require('fastify-plugin')

module.exports = fp(async (fastify) => {
  fastify.decorate('cleansData', (data) => {
    const obj = data
    const notAllowed = ['password', '_id', '__v', 'va_type']
    const keys = Object.keys(obj)

    notAllowed.forEach((na) => {
      if (keys.filter((key) => na === key).length > 0) {
        delete obj[na]
      }
    })

    return obj
  })
})
