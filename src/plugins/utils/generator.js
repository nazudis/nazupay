const fp = require('fastify-plugin')
const { pbkdf2Sync } = require('crypto')

const SALT = process.env.ENC_KEY

module.exports = fp(async (fastify) => {
  fastify.decorate('genPassword', (plaintext) =>
    pbkdf2Sync(plaintext, SALT, 100, 64, 'sha512').swap64().toString('base64')
  )

  fastify.decorate(
    'comparePassword',
    (plaintext, password) => fastify.genPassword(plaintext) === password
  )

  fastify.decorate('genApiKey', (payload) => {
    const tmpkey = Buffer.from(payload).swap16().toString('base64')
    let key = ''
    tmpkey
      .split('')
      .reverse()
      .forEach((s, i) => {
        key += s
        if ((i + 1) % 6 === 0 && i !== tmpkey.length - 1) key += '-'
      })

    return key
  })

  fastify.decorate('decodeApiKey', (apikey) => {
    const key = apikey.split('-').join('').split('').reverse().join('')
    const plain = Buffer.from(key, 'base64').swap16().toString('utf-8')

    return plain
  })

  fastify.decorate('genPartnerId', async () => {
    const partnerId = `${Math.floor(10000 + Math.random() * 900000)}`
    const isExists = await fastify.PartnerModel.findOne({
      partner_id: partnerId,
    })

    if (isExists || partnerId.length < 6) {
      fastify.genPartnerId()
    }

    return partnerId
  })

  fastify.decorate('genTrxId', async (partnerId, trxFor, prefix) => {
    const startDay = fastify.dayjs().startOf('day').toDate()
    const today = fastify.dayjs().format('YYMMDD')
    let seri = '00000'
    let todayTransaction = 0
    let trxId = ''

    if (trxFor === 'partner') {
      todayTransaction = await fastify.VaModel.countDocuments({
        partner_id: partnerId,
        created_at: { $gt: startDay },
      })

      trxId = `${prefix}-${partnerId}`
    }

    if (trxFor === 'invoice') {
      todayTransaction = await fastify.TrxModel.countDocuments({
        partner_id: partnerId,
        created_at: { $gt: startDay },
      })

      trxId = `INV-${partnerId}`
    }

    seri = `${seri}${todayTransaction + 1}`.slice(-6)

    return `${trxId}-${today}${seri}`
  })

  fastify.decorate('genVA', async (partnerId) => {
    const vaNumber = Math.floor(10000000 + Math.random() * 90000000)
    const isExists = await fastify.VaModel.findOne({
      partner_id: partnerId,
      va_number: vaNumber,
    })
    if (isExists) {
      fastify.genVA(partnerId)
    }

    return `88${vaNumber}${partnerId}`
  })
})
