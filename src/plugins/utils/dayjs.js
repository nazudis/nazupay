const DayJs = require('dayjs')
const utc = require('dayjs/plugin/utc')
const timezone = require('dayjs/plugin/timezone')
const updateLocale = require('dayjs/plugin/updateLocale')
const localeId = require('dayjs/locale/id')
const fp = require('fastify-plugin')

module.exports = fp(async (fastify) => {
  fastify.decorate('dayjs', (date) => {
    DayJs.extend(utc)
    DayJs.extend(timezone)
    DayJs.extend(updateLocale)
    DayJs.locale(localeId)

    return DayJs.tz(date || DayJs(), 'Asia/Jakarta')
  })
})
