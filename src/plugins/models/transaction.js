const fp = require('fastify-plugin')
const { Schema, model } = require('mongoose')

const TransactionSchema = new Schema(
  {
    client_name: String,
    trx_status: {
      type: String,
      enum: ['SUCCESS', 'FAILED'],
    },
    trx_id: {
      type: String,
      required: true,
      unique: true,
    },
    trx_amount: {
      type: Number,
      required: true,
    },
    trx_time: {
      type: Date,
    },
    va_number: {
      type: String,
      required: true,
    },
    partner_id: {
      type: String,
      required: true,
    },
    partner_trx_id: {
      type: String,
      required: true,
    },
    description: String,
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
)

module.exports = fp(async (fastify) => {
  fastify.decorate('TrxModel', model('Transactions', TransactionSchema))
})
