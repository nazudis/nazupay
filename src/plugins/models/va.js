const fp = require('fastify-plugin')
const { Schema, model } = require('mongoose')

const VASchema = new Schema(
  {
    client_name: String,
    va_type: {
      type: String,
      enum: ['O', 'M'],
    },
    va_status: {
      type: String,
      enum: ['ACTIVE', 'INACTIVE'],
      default: 'ACTIVE',
    },
    va_number: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
      default: 0,
    },
    partner_id: {
      type: String,
      required: true,
    },
    partner_trx_id: {
      type: String,
      required: true,
      unique: true,
    },
    description: String,
    exp_time: {
      type: Date,
    },
    callback: String,
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
)

module.exports = fp(async (fastify) => {
  fastify.decorate('VaModel', model('VirtualAccounts', VASchema))
})
