const { Schema, model } = require('mongoose')
const fp = require('fastify-plugin')

const UserSchema = new Schema(
  {
    company: {
      type: String,
      required: true,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    partner_id: {
      type: String,
      required: true,
      unique: true,
    },
    expired_key: Date,
    last_access: Date,
    is_active: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
)

module.exports = fp(async (fastify) => {
  fastify.decorate('PartnerModel', model('Partners', UserSchema))
})
