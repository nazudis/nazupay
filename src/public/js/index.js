// DOM Selector and Set
const formva = document.querySelector('#searchva')
const formpayment = document.querySelector('#payment')
const detailElm = document.querySelectorAll('.detail-va')
const errorsearch = document.querySelector('#errorsearch')
const notifElm = document.querySelector('#notif')

const setTextContent = (id, value) =>
  (document.querySelector(`#${id}`).textContent = value)

// Utils Function
const getCookie = (name) => {
  const match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'))
  if (match) return match[2]
}

const Buffer = buffer.Buffer
const { apikey, partnerId } = JSON.parse(
  Buffer.from(getCookie('keys'), 'hex').toString('ascii')
)

const headers = {
  'x-api-key': apikey,
  'x-partner-id': partnerId,
  'Content-Type': 'application/json',
}

// Listener
formva.addEventListener('submit', async (e) => {
  e.preventDefault()

  const va = formva.virtualaccount.value
  const res = await (await fetch(`/api/va/detail/${va}`, { headers })).json()
  if (res.status.code === '000') {
    setTextContent('name', res.client_name)
    setTextContent('va', res.va_number)
    setTextContent('amount', `Rp. ${res.amount.toLocaleString('IN')},-`)
    setTextContent('desc', res.description)

    formva.virtualaccount.value = ''

    detailElm.forEach((elm) => {
      elm.classList.remove('hidden')
    })
  } else {
    errorsearch.textContent = res.status.message
  }
})

formva.virtualaccount.oninput = () => {
  if (errorsearch.textContent) {
    errorsearch.textContent = ''
  }
}

formpayment.addEventListener('submit', async (e) => {
  e.preventDefault()

  const va = document.querySelector('#va').textContent
  const amount = formpayment.amount.value

  const res = await (
    await fetch('/api/transaction/create', {
      method: 'post',
      headers,
      body: JSON.stringify({ va_number: va, amount }),
    })
  ).json()

  // Element For Notification
  const notifChild = document.createElement('div')
  notifChild.classList.add(
    'p-2',
    'rounded',
    'flex',
    'flex-row',
    'gap-16',
    'transition',
    'transition-opacity',
    'duration-300',
    'ease-in-out',
    'opacity-0'
  )

  notifChild.innerHTML = `<p class="flex-grow">${res.status.message}</p>
  <p class='font-bold justify-self-end cursor-pointer flex-none'>4s</p>`

  if (res.status.code === '000') {
    notifChild.classList.add('bg-green-400')
  } else {
    notifChild.classList.add('bg-red-400')
  }

  formpayment.amount.value = ''
  notifElm.append(notifChild)

  let sec = 3
  const timer = setInterval(() => {
    if (sec === 0) {
      clearInterval(timer)
    }
    notifChild.lastChild.textContent = `${sec}s`
    sec--
  }, 1000)

  setTimeout(
    () => notifChild.classList.replace('opacity-0', 'opacity-100'),
    500
  )
  setTimeout(
    () => notifChild.classList.replace('opacity-100', 'opacity-0'),
    4500
  )
  setTimeout(() => notifElm.removeChild(notifChild), 4800)
})
