#!/bin/bash

cd /home/ubuntu/develop/nazupay
git checkout .
git checkout master
git pull origin master

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
npm install --only=prod
pm2 restart pm2.json